import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class WeatherCityService {
  public city: Subject<any> = new Subject<any>();
  public forecast: Subject<any> = new Subject<any>();

  constructor(private http: HttpClient) { }

  public getWeatherForCity(name: string): void {
    if (name && name.trim().length) {
      const params = new HttpParams().set('q', name);

      this.http.get('https://api.openweathermap.org/data/2.5/weather?APPID=d8f280ea9d8ad17fb0fc4ec00f110575',
        { params: params })
        .subscribe(data => {
          this.city.next(data);
        });
    }
  }

  public getForecastForCity(id: string): void {
    if (id) {
      const params = new HttpParams().set('id', id);

      this.http.get('https://api.openweathermap.org/data/2.5/forecast?APPID=d8f280ea9d8ad17fb0fc4ec00f110575',
        { params: params })
        .subscribe(data => {
          this.forecast.next(data);
        });
    }
  }

}
