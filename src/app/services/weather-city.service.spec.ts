import { TestBed, inject } from '@angular/core/testing';

import { WeatherCityService } from './weather-city.service';

describe('WeatherCityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WeatherCityService]
    });
  });

  it('should be created', inject([WeatherCityService], (service: WeatherCityService) => {
    expect(service).toBeTruthy();
  }));
});
