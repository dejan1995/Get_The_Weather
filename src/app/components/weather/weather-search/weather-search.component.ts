import {Component, EventEmitter, OnInit, Output, ElementRef, ViewChild} from '@angular/core';
import { WeatherCityService } from './../../../services/weather-city.service';

@Component({
  selector: 'app-weather-search',
  templateUrl: './weather-search.component.html',
  styleUrls: ['./weather-search.component.scss']
})
export class WeatherSearchComponent implements OnInit {
  public cities = [];
  public isCitySerched = false;

  @ViewChild('searchInput')
  private searchInput: ElementRef;

  @Output()
  private emitSearchedCity: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  private emitRemovedCity: EventEmitter<any> = new EventEmitter<any>();

  constructor (private weatherCityService: WeatherCityService) { }

  ngOnInit() {
    this.weatherCityService.city.subscribe((data) => {
      this.cities = [...this.cities, data];
      this.emitSearchedCity.emit(data);
    });
  }

  public emitSearchQuery(event: string): void {
    const multipleCities = event.split(', ');

    for (const c of multipleCities) {
      c.trim();

      for (const city of this.cities) {
        if (c === city.name.toLowerCase()) {
          this.isCitySerched = true;
          alert(`You have already searched for ${city.name}!`);

          break;
        } else {
          this.isCitySerched = false;
        }
      }

      if (!this.isCitySerched) {
        this.weatherCityService.getWeatherForCity(c);
      }
    }

    this.searchInput.nativeElement.value = '';
  }

  public removeCityTag(cityForRemoval: string): void {
    this.cities = this.cities.filter(city => city.name !== cityForRemoval);
    this.emitRemovedCity.emit(cityForRemoval);
  }
}
