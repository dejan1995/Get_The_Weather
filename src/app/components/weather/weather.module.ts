import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {routing} from './weather.routing';
import {WeatherListComponent} from './weather-list/weather-list.component';
import {WeatherSearchComponent} from './weather-search/weather-search.component';
import { WeatherCityService } from './../../services/weather-city.service';
import { WeatherCityComponent } from './weather-city/weather-city.component';
import {AgmCoreModule} from '@agm/core';


@NgModule({
  imports: [
  CommonModule,
    routing,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDD9O-R5diKuR_KAeGdhqchfcV_VLbHFBI'
    })
  ],
  declarations: [
    WeatherListComponent,
    WeatherSearchComponent,
    WeatherCityComponent
  ],
  providers: [WeatherCityService]
})
export class WeatherModule {}
