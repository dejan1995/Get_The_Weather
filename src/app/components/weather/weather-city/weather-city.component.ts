import { Component, OnInit } from '@angular/core';
import { WeatherCityService } from './../../../services/weather-city.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-weather-city',
  templateUrl: './weather-city.component.html',
  styleUrls: ['./weather-city.component.scss']
})
export class WeatherCityComponent implements OnInit {

  public forecast: any;
  public forecastPerDay = [];
  public latitude = null;
  public longitude = null;

  constructor(private weatherCityService: WeatherCityService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    const cityId = this.activatedRoute.snapshot.params['id'];
    this.weatherCityService.getForecastForCity(cityId);
    this.weatherCityService.forecast.subscribe(data => {
      this.forecast = data;

      for (const index in this.forecast.list) {
        if (this.forecast.list.hasOwnProperty(index)) {
          if (+index % 8 === 0) {
            this.forecastPerDay = [...this.forecastPerDay, this.forecast.list[index]];
          }
        }
      }

      this.latitude = this.forecast.city.coord.lat;
      this.longitude = this.forecast.city.coord.lon;
      console.log(data);
    });
  }

}
