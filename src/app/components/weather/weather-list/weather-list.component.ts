import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-weather-list',
  templateUrl: './weather-list.component.html',
  styleUrls: ['./weather-list.component.scss']
})
export class WeatherListComponent implements OnInit {

  public cityList = [];

  constructor() { }

  ngOnInit() {
  }

  public addCityToWeatherList(city: any): void {
    console.log(city);
    this.cityList = [...this.cityList, city];
  }

  public removeCityFromWeatherList(cityForRemoval: any): void {
    this.cityList = this.cityList.filter(city => city.name !== cityForRemoval);
  }
}
