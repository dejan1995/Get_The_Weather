import {Routes, RouterModule} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {WeatherListComponent} from './weather-list/weather-list.component';
import { WeatherCityComponent } from './weather-city/weather-city.component';

const routes: Routes = [
  { path: '', component: WeatherListComponent },
  { path: 'city/:id', component: WeatherCityComponent}
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
